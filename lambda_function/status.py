import os

is_provisioned_concurrency = (
    os.environ.get("AWS_LAMBDA_INITIALIZATION_TYPE")
    == "provisioned-concurrency"
)
