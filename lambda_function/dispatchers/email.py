from hashlib import sha1
import time

import boto3
from botocore.exceptions import ClientError

from aws_xray_sdk.core import xray_recorder

from .base import Dispatcher

from ..logging import getLogger
logger = getLogger(__name__)

BATCH_SIZE = 50
CHARSET = "UTF-8"
SENDER = "GraceDB <gracedb@gravitationalwave.services>"


_ses_client = None


class EmailDispatcher(Dispatcher):
    @xray_recorder.capture("## init_email")
    def __init__(self, tier):
        global _ses_client
        if _ses_client is None:
            time_start = time.perf_counter()

            _ses_client = boto3.client('ses')

            time_elapsed = time.perf_counter() - time_start
            logger.info("Connected AWS SES client in %.1f sec", time_elapsed)

        else:
            logger.info("Using existing AWS SES client")

        self.client = _ses_client
        self.snsArn = ''

    @xray_recorder.capture("## handle_email")
    def _handle(self, contents, context) -> tuple[int, str]:
        recipients = contents.recipients
        subject = contents.subject
        body = contents.body

        message_id = sha1(
            (
                ",".join(recipients) + subject + body
            ).encode('utf-8')
        ).hexdigest()

        for index_start in range(0, len(recipients), BATCH_SIZE):
            bcc_addresses = recipients[index_start:index_start+BATCH_SIZE]
            bcc_addresses_str = ", ".join(bcc_addresses)

            logger.info("Emailing %s with message_id %s",
                        bcc_addresses_str, message_id)

            try:
                self.client.send_email(
                    Destination={
                        'BccAddresses': bcc_addresses,
                    },
                    Message={
                        'Body': {
                            'Text': {
                                'Charset': CHARSET,
                                'Data': body,
                            },
                        },
                        'Subject': {
                            'Charset': CHARSET,
                            'Data': subject,
                        },
                    },
                    Source=SENDER,
                )
            except ClientError as e:
                logger.error("Failed to send email with message_id %s: %s",
                             message_id, e.response['Error']['Message'])
            else:
                logger.info("Emailed %s with message_id %s",
                            bcc_addresses_str, message_id)

        return 200, "Success"
